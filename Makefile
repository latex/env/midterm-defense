#
# Use this Makefile with GNU make.
#
TOP ?= 20xx-THYME-MidDefense-report.pdf  20xx-THYME-MidDefense-slides.pdf
TOP_NAME = $(patsubst %.pdf,%,$(TOP))
# EXT = pdf log aux out bbl blg toc snm nav fdb_latexmk fls

STYS       = $(wildcard texinputs/*.sty)

FIGS       = $(wildcard figs/*.fig)
FIGS_PDF   = $(patsubst %.fig,%.pdftex,$(FIGS))
FIGS_PDF_T = $(patsubst %.fig,%.pdftex_t,$(FIGS))

SVGS       = $(wildcard svgs/*.svg)
SVGS_PDF   = $(patsubst %.svg,%.pdf,$(SVGS))

ODGS       = $(wildcard odgs/*.odg)
ODGS_PDF   = $(patsubst %.odg,%.pdf,$(ODGS))

SIMS_MAT 	= $(wildcard results_with_simulation_code/*.m)
SIMS_MAT_PDF= $(patsubst %.m,%.pdf,$(SIMS_MAT))

NEEDED = $(FIGS_PDF) $(FIGS_PDF_T) $(SVGS_PDF) $(ODGS_PDF) # $(SIMS_MAT_PDF)

export TEXINPUTS := ./texinputs/:$(TEXINPUTS)
export BIBINPUTS := ./texinputs/:$(BIBINPUTS)
export BSTINPUTS := ./texinputs/:$(BSTINPUTS)

#.SECONDARY: $(FIGS_PDF)

.PHONY: all clean dep preview

all: $(TOP)

$(TOP) : dep

dep: $(NEEDED) 

%.pdf:%.tex $(STYS)
	@echo "LaTeX search path $(TEXINPUTS)"
	@latexmk -pdf $<

preview: dep
	pdflatex  '$(PREVIEW_OPTS) \input{$(TOP_NAME).tex}'

clean: .latexmkrc
	latexmk -c $(TOP_NAME)

cleaner: .latexmkrc
	@echo Cleaning $(TOP) and $(NEEDED)
	latexmk -C $(TOP_NAME)
	rm -f $(NEEDED)
	

%.pdftex:%.fig
	fig2dev -L pdftex $< $@
%.pdftex_t:%.pdftex
	fig2dev -L pdftex_t -p $< $(patsubst %.pdftex_t,%.fig,$@) $@

%.swf:%.pdf
	pdf2swf $< && chmod -x $@

# Get Inkscape version
INKSCAPEVERSION = $(shell inkscape --version | cut -f2 -d" ")
# inkscapever -> x.yy.z
INKSCAPEVERSION_MAJOR = $(shell echo $(INKSCAPEVERSION) | cut -f1 -d.)

%.pdf:%.svg
	if [ "$(INKSCAPEVERSION_MAJOR)" = "0" ]; \
	then \
		inkscape -f $< --export-pdf=$@ ; \
	else \
		inkscape --export-type=pdf -o $@ $< ; \
	fi

%.pdf:%.odg
	libreoffice --headless --convert-to pdf $< --outdir odgs
	pdfcrop --margins 1 $@ $@

%.pdf:%.m
	matlab -batch "addpath(genpath('results_with_simulation_code')); $(notdir $*) "
	mv $(notdir $@) $@

# You shoul run this target once to set up latexmk for extended cleaning
initlatexmkrc: .latexmkrc
	@echo 'setting up latexmk for extended cleaning'
	@echo '$$clean_ext = "bbl nav out snm run.xml";' > .latexmkrc

.latexmkrc:
	touch .latexmkrc