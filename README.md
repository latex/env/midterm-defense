# MidTerm Defense Report+Slides Environment

## Getting started

Please clone with : `--recurse-submodules` option

```bash
git clone --recurse-submodules git@gitlab.enst.fr:latex/env/mid-term-defense-report-slides-build-environment.git"
```

## Dependencies

- `pdflatex`
- `latexmk`
- GNU Make
- `libreoffice`
- `inkscape`
